package hrytsenko;

import lombok.SneakyThrows;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Component
    public static class IpAddressInfo implements InfoContributor {
        @Override
        @SneakyThrows
        public void contribute(Info.Builder builder) {
            builder.withDetail("name", InetAddress.getLocalHost().getHostName())
                    .withDetail("ip", InetAddress.getLocalHost().getHostAddress());
        }
    }

}
