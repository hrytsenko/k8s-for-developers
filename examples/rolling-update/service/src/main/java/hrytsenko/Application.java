package hrytsenko;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Slf4j
    @Component
    public static class ReadinessProbe implements HealthIndicator {
        @Override
        public Health health() {
            log.info("Readiness probe done");
            return Health.up().build();
        }
    }

}
