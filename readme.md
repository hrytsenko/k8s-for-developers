# About

Source code for the training 'k8s-for-developers'.
Documentation: [Confluence](https://hrytsenko.atlassian.net/wiki/spaces/KUBERNETES/).

# License

[Apache License 2.0](LICENSE)
